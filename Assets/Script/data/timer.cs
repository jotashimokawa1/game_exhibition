using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;//EditorApplicationを使うの必要

public class timer : MonoBehaviour
{
    [SerializeField] float m_time = 0f;
    private bool is_stop = false;

    private float onetime = 0f;

    public bool IsTImerStart = false;

    private void Update()
    {
        if(IsTImerStart == true)
        {
            onetime += Time.deltaTime;
            m_time += Time.deltaTime;

            if (onetime >= 1f)
            {
                ShowTimerResult();
                onetime = 0;
            }
        }



    }

    private void ShowTimerResult()
    {
        //どのステージまで進んだかを取得
        int stage = StageManager.Instance.CurrentCheckPoint;

        int sec = (int)m_time;

        Debug.Log(" クリア時間は " + sec.ToString() + "秒　最終ステージは" + stage.ToString() + "です。");
    }

    private void OnTriggerEnter(Collider other)
    {
        IsTImerStart = true;
    }

    public void TimerStop()
    {
        IsTImerStart = false;
    }

}
