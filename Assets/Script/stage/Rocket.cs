using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Rocket : Success
{
    public GameObject cat;
    public bool endgame = false;
    public GameObject clearCanvas;

    public ParticleSystem red;
    public ParticleSystem yellow;


    private void Start()
    {
        //ロケットを非表示にする
        MeshRenderer[] meshs = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer nowm in meshs)
        {
            nowm.enabled = false;
        }
        clearCanvas.SetActive(false);

        red.Stop();
        yellow.Stop();
    }

    private void Update()
    {
        if (endgame)
        {
            SuccessMethod();
            endgame = false;
        }
    }

    public override void SuccessMethod()
    {
        //クリアの音を流す
        BGMManager.Instance.PlaySE(BGMManager.SE.CheckPoint, cat);

        //ネコの操作権を失う
        newCharCont charcont = cat.GetComponent<newCharCont>();
        charcont.enabled = false;
        cat.transform.position = this.transform.position;

        //画面を暗転させる（本当は乗車させてあげたいけど、場所がわからないのでやめる） やっぱ暗転辞める

        //ブロックの表示を消す
        List<Block> blocklist = BlockGroupManager.Instance.BlockList;
        foreach(Block nowBlock in blocklist)
        {
            nowBlock.gameObject.GetComponent<MeshRenderer>().enabled = false;
        }

        //ロケットを表示する
        MeshRenderer[] meshs = GetComponentsInChildren<MeshRenderer>();
        foreach(MeshRenderer nowm in meshs)
        {
            nowm.enabled = true;
        }

        //ロケットを飛ばす(どこまでも?)
        this.transform.DOMove(this.transform.position + new Vector3(0, 10000f, 0f), 60f).SetEase(Ease.InOutQuart).OnUpdate(() =>
        {
            cat.transform.position = this.transform.position;
        });;

        //ロケットに火をつける
        red.Play();
        red.gameObject.transform.DOScale(new Vector3(250, 250, 250), 10);
        yellow.Play();
        yellow.gameObject.transform.DOScale(new Vector3(250, 250, 250), 10);

        //ゲームクリアを表示
        StartCoroutine("ViewClearCanvas");

    }

    IEnumerator ViewClearCanvas()
    {
        yield return new WaitForSeconds(10);
        clearCanvas.gameObject.SetActive(true);
    }


}
