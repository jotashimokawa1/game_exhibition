using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OpenDoor : Success
{
    public override void SuccessMethod()
    {
        //base.SuccessMethod();

        BGMManager.Instance.PlaySE(BGMManager.SE.CheckPoint, this.gameObject, 0.2f, 1f);
        BGMManager.Instance.PlaySE(BGMManager.SE.Door, this.gameObject, 0.6f, 1f);

        transform.DOMove(new Vector3(-737, 796, -2166), 4);
    }

}
