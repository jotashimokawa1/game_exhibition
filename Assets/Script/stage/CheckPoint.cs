using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    [SerializeField] private int stagenum;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            StageManager.Instance.CurrentCheckPoint = stagenum;
            BGMManager.Instance.PlaySE(BGMManager.SE.CheckPoint, Camera.main.gameObject,0.2f,1f);
        }
    }

}
