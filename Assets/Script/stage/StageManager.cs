using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : Singleton<StageManager>
{
    [SerializeField] private GameObject Player;
    [SerializeField] private List<GameObject> stages;
    [SerializeField]private int m_CurrentCheckpoint = 0;

    public int CurrentCheckPoint
    {
        get { return m_CurrentCheckpoint; }
        set { m_CurrentCheckpoint = value; }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Player.GetComponent<CharacterController>().enabled = false;
            GoCheckPoint(CurrentCheckPoint);
            Player.GetComponent<CharacterController>().enabled = true;
        }
    }

    //プレイヤーをチェックポイントへ移動
    public void GoCheckPoint(int stagenum)
    {
        Player.transform.position = stages[stagenum].transform.position;
    }
}
