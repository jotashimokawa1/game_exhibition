using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnTrackingTexture : MonoBehaviour
{
    //  [SerializeField, ReadOnly]
    // private Material attentionMaterial;
    // private Material attentionkeep;
    // Start is called before the first frame update

    MeshRenderer renderer;
    [SerializeField]
    Material matBase;
    [SerializeField]
    Material matAttention;

    Material[] mats = new Material[2];
    

    void Start()
    {
        renderer = this.gameObject.GetComponent<MeshRenderer>();
        mats[0] = matBase;
        mats[1] = matAttention;
        renderer.materials = mats;
        
       
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
