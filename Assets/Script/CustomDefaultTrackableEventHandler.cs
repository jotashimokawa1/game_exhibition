using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Vuforia;

public class CustomDefaultTrackableEventHandler : DefaultObserverEventHandler
{
    public UnityEvent ExtendTrackingAction_find;
    public UnityEvent ExtendTrackingAction_lost;



    protected override void OnObserverStatusChanged(ObserverBehaviour behaviour, TargetStatus targetStatus)
    {
        base.OnObserverStatusChanged(behaviour, targetStatus);

        if (targetStatus.Status == Status.EXTENDED_TRACKED)
        {
            //見失った
            ExtendTrackingAction_lost.Invoke();
            Debug.Log("ExtendTrackingAction_Lost");
        }
        if(targetStatus.Status == Status.TRACKED)
        {
            //見つけた
            ExtendTrackingAction_find.Invoke();
            Debug.Log("ExtendTrackingAction_Find");

        }
    }

    
}
