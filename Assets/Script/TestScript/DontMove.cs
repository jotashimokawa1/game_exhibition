using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontMove : MonoBehaviour
{
    public bool isStopped;
    public bool switchButton;
    private Transform trans;
    private Vector3 pos;
    private Quaternion rot;
    // Start is called before the first frame update
    void Start()
    {
        switchButton = false;
        isStopped = false;
        trans = this.gameObject.GetComponent<Transform>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if(switchButton== true)
        {
            pos = trans.position;
            rot = trans.rotation;
            switchButton = false;
            isStopped = !isStopped;
        }
        if (isStopped)
        {
            trans.position = pos;
            trans.rotation = rot;
        }
        
    }
}
