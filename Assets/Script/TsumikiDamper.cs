using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TsumikiDamper : MonoBehaviour
{
    private Transform myTrans;
    [SerializeField]
    private Transform parentTrans;
    public float distanceThreshold;
   // [SerializeField]
  //  private float rotationThreshold = 0.99f;

    private Rigidbody rb;
    [SerializeField, ReadOnly]
    public bool parentMoving = true;

    private Vector3 parentPosCash;
    Quaternion firstRot;



    // Start is called before the first frame update
    void Start()
    {
        myTrans = this.gameObject.GetComponent<Transform>();
        firstRot = myTrans.rotation;
        rb = this.gameObject.GetComponent<Rigidbody>();
        parentPosCash = parentTrans.position;

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //parentの位置・姿勢が自分よりいくらか離れたら，そこに移動する（振動防止）  ※内積で角度の差がでる．体感rotationThreshold = 0.995で5°以内くらい
        // if ((parentTrans.position - parentPosCash).magnitude >= distanceThreshold || Quaternion.Dot(parentTrans.rotation, myTrans.rotation) <= rotationThreshold)
        if ((parentTrans.position - parentPosCash).magnitude >= distanceThreshold)
        {
            //Debug.Log("distance:" + (parentTrans.position - parentPosCash).magnitude + "*dot: " + Quaternion.Dot(parentTrans.rotation, myTrans.rotation));
            //現実の積み木は動いている
            myTrans.position = parentTrans.transform.position;
            myTrans.rotation = parentTrans.transform.rotation * firstRot;
            parentPosCash = parentTrans.position;
            //開発用←これだとすぐ消える
            parentMoving = true;
        }
        else
        {
            //現実の積み木が止まっている
            rb.isKinematic = false;
            rb.useGravity = true;
            //開発用
            parentMoving = false;
        }
       
        
        





    }
}
