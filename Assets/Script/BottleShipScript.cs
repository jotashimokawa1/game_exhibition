using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;



public class BottleShipScript : MonoBehaviour
{

    [SerializeField]
    private GameObject AppearanceBlock;

    public bool removeExistingCollider = true;


    // Start is called before the first frame update
    void Start()
    {
        CreateInvertedMeshCollider();

    }

 
    
    public void CreateInvertedMeshCollider()
    {
        if (removeExistingCollider)
        {
            //今のColliderを消す
            RemoveExistingColliders();

        }

        InvertMesh();
        MeshCollider mcol= gameObject.AddComponent<MeshCollider>();

        //反転したColliderをColliderSettingに渡す
        // recognitionObj.GetComponent<ColliderSetting>().colB = gameObject.GetComponent<MeshCollider>();

        mcol.enabled = true;
        //  Debug.Log(mcol.enabled);
        AppearanceBlock.GetComponent<OriginalBlock>().AreaCol = mcol;

    }

    ///<summary> "aa" </summary>
    private void RemoveExistingColliders()
    {
        Collider[] colliders = GetComponents<Collider>();
        for (int i = 0; i < colliders.Length; i++)
            DestroyImmediate(colliders[i]);
    }

    private void InvertMesh()
    {
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        mesh.triangles = mesh.triangles.Reverse().ToArray();
        

    }
    
    
}
