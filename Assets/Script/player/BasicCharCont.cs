﻿using UnityEngine;
using System.Collections;

public class BasicCharCont : MonoBehaviour
{

    [SerializeField] private float walkSpeed = 2.0f; // 歩行速度
    [SerializeField] private float runSpeed = 6.0f; // 走行速度

    private Vector3 movement;   // 移動するベクター

    public float gravity = 20.0f; // キャラへの重力
    private float speedSmoothing = 10.0f;   // 回頭するときの滑らかさ
    private float rotateSpeed = 500.0f; // 回頭の速度
    private float runAfterSeconds = 0.1f;   // 走り終わったとき止まるまでの時間(秒)

    private Vector3 moveDirection = Vector3.zero;   // カレント移動方向
    private float verticalSpeed = 0.0f;    // カレント垂直方向速度
    private float moveSpeed = 0.0f;    // カレント水平方向速度

    private CollisionFlags collisionFlags;    //  controller.Move が返すコリジョンフラグ：キャラが何かにぶつかったとき使用

    private float walkTimeStart = 0.0f;    // 歩き始める速度

    CharacterController controller;
    public float jumpPower;

    private bool canjump = true;
    Animator anim;

    // Use this for initialization
    void Start()
    {
        moveDirection = transform.TransformDirection(Vector3.forward);  // キャラの移動方向をキャラの向いている方向にセットする
        controller = GetComponent<CharacterController>();   // キャラクターコントローラコンポーネントを取得
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Transform cameraTransform = Camera.main.transform;  // カメラの向いている方向を得る
        Vector3 forward = cameraTransform.TransformDirection(Vector3.forward);  // camera の x-z 平面から forward ベクターを求める 
        forward.y = 0;  // Y方向は無視：キャラは水平面しか移動しないため
        forward = forward.normalized;   // 方向を正規化する
        Vector3 right = new Vector3(forward.z, 0, -forward.x);    // 右方向ベクターは常にforwardに直交

        float v = Input.GetAxisRaw("Vertical"); // マウスもしくはコントローラスティックの垂直方向の値
        float h = Input.GetAxisRaw("Horizontal");   // マウスもしくはコントローラスティックの水平方向の値

        Vector3 targetDirection = h * right + v * forward;  // カメラと連動した進行方向を計算：視点の向きが前方方向



        if ((collisionFlags & CollisionFlags.CollidedBelow) != 0)   // キャラは接地しているか？：宙に浮いていないとき
        {
            if (targetDirection != Vector3.zero) // キャラは順方向を向いていないか？：つまり回頭している場合
            {
                if (moveSpeed < walkSpeed * 0.9) // ゆっくり移動か？
                {
                    moveDirection = targetDirection.normalized; // 止まっているときは即時ターン
                }
                else  // 移動しているときはスムースにターン
                {

                    //現在の位置からターゲットに向けてベクトルを回転させる。(現在地、目的地、回転できる最大の角度量、許容できる回転の平方根の長さ)
                    moveDirection = Vector3.RotateTowards(moveDirection, targetDirection, rotateSpeed * Mathf.Deg2Rad * Time.deltaTime, 1000);
                    moveDirection = moveDirection.normalized;
                    anim.SetInteger("Walk", 1);

                }
            }

            float curSmooth = speedSmoothing * Time.deltaTime;     // 向きをスムースに変更
            float targetSpeed = Mathf.Min(targetDirection.magnitude, 1.0f); // 最低限のスピードを設定

            // 歩く速度と走る速度の切り替え：最初は歩いていて時間がたつと走る
            if (Time.time - runAfterSeconds > walkTimeStart)
                targetSpeed *= runSpeed;
            else
                targetSpeed *= walkSpeed;

            // 移動速度を設定
            moveSpeed = Mathf.Lerp(moveSpeed, targetSpeed, curSmooth);///Leap（開始値、終了値、補完値）


            if (moveSpeed < walkSpeed * 0.3)    // まだ歩きはじめ
                walkTimeStart = Time.time;  // その時間を保存しておく

            verticalSpeed = 0.0f;   // 垂直方向の速度をゼロに設定

            canjump = true;

            if (moveSpeed <= 5)
            {
                anim.SetInteger("Walk", 0);
            }

        }
        else // 宙に浮いている
        {
            verticalSpeed -= gravity * Time.deltaTime;  // 重力を適応
        }

        //スペースでジャンプする
        if ((Input.GetKeyDown(KeyCode.Space)) && canjump)
        {
            verticalSpeed += jumpPower;
            moveDirection = moveDirection / 2; //ジャンプ中は平面移動を少なくする。/2は適当なので後で調節する
            canjump = false;
            anim.SetTrigger("jump");
        }


        movement = moveDirection * moveSpeed + new Vector3(0, verticalSpeed, 0);   // キャラの移動量を計算
        movement *= Time.deltaTime;

        collisionFlags = controller.Move(movement);   // キャラを移動をキャラクターコントローラに伝える


        if ((collisionFlags & CollisionFlags.CollidedBelow) != 0)   // 宙に浮いてい無ければ移動方向に回頭
        {
            transform.rotation = Quaternion.LookRotation(moveDirection);
        }

        if (transform.position.y < -700f)
        {
            StageManager.Instance.GoCheckPoint(StageManager.Instance.CurrentCheckPoint);
        }

    }
}