using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckerOn : MonoBehaviour
{
    public ShapeChacker shapechacker;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            shapechacker.enabled = true;

        }
    }
}
