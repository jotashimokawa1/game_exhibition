using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ShapeChacker : MonoBehaviour
{
    [SerializeField] private float ShapePoint;
    [SerializeField] private float SquarePoint;
    [SerializeField] private float RectanglePoint;
    [SerializeField] private float TrianglePoint;
    [SerializeField] private float NierPoint; //似ているかどうかの閾値

    public GameObject CentorBlock; //中心点となるブロック（真ん中にあるとは限らない）
    [SerializeField] private Shape.ShapeType CentorType;
    [SerializeField] private List<GameObject> AnswerBlocks; //事前登録する

   // [SerializeField] private List<Vector3> AnswerPosition; //デバッグ用。本番ではserializefieldは消す

    //正解ブロックを形ごとに分類わけ。スクリプトから代入
    [SerializeField] private List<Vector3> AnswerSquareBlocksPos = new List<Vector3>();
    [SerializeField] private List<Vector3> AnswerRectangleBlocksPos = new List<Vector3>();
    [SerializeField] private List<Vector3> AnswerTriangleBlocksPos = new List<Vector3>();
    [SerializeField] private List<Vector3> AnswerSquareBlocksAngle = new List<Vector3>();
    [SerializeField] private List<Vector3> AnswerRectangleBlocksAngle = new List<Vector3>();
    [SerializeField] private List<Vector3> AnswerTriangleBlocksAngle = new List<Vector3>();

    //縦横奥行の誤差の優先度。例えば縦の誤差は許容しないほうが良い。
    [SerializeField, Range(0f, 1f)] private float priorityPos = 0.5f;
    [SerializeField] private float priorityPosX = 1;
    [SerializeField] private float priorityPosY = 1;
    [SerializeField] private float priorityPosZ = 1;
    [SerializeField] private float priorityAngleX = 1;
    [SerializeField] private float priorityAngleY = 1;
    [SerializeField] private float priorityAngleZ = 1;

    [SerializeField] private float PositionDiffMax = 300; //300以上離れている場合は１点になる(評価しないでもいい)

    //  [SerializeField] private List<GameObject> PlayerBlocks; //testでは最初から代入されている。サーチ系の関数を作る
    [SerializeField] private List<List<GameObject>> CentorGroups = new List<List<GameObject>>(); //センターブロックが含まれているグループのリスト
    private float time = 0;//test用

    //テスト
    public bool is_test;

    /* public GameObject answersblock;
     public GameObject playersblock;
     public GameObject playerscentor;*/

    public Success success;

    void Start()
    {
        //センターブロックの形を保存
        CentorType = CentorBlock.GetComponent<Shape>().shapetype;

        //センターとの座標の差分を記録
        foreach (GameObject nowobj in AnswerBlocks)
        {
            Vector3 difPos = nowobj.transform.position - CentorBlock.transform.position;

            switch (nowobj.GetComponent<Shape>().shapetype) 
            {
                case Shape.ShapeType.square : AnswerSquareBlocksPos.Add(difPos); break;
                case Shape.ShapeType.rectangle : AnswerRectangleBlocksPos.Add(difPos); break;
                case Shape.ShapeType.triangle : AnswerTriangleBlocksPos.Add(difPos); break;
            }

            switch (nowobj.GetComponent<Shape>().shapetype)
            {
                case Shape.ShapeType.square: AnswerSquareBlocksAngle.Add(Vector3.Cross(CentorBlock.transform.forward, nowobj.transform.forward)); break;
                case Shape.ShapeType.rectangle: AnswerRectangleBlocksAngle.Add(Vector3.Cross(CentorBlock.transform.forward, nowobj.transform.forward)); break;
                case Shape.ShapeType.triangle: AnswerTriangleBlocksAngle.Add(Vector3.Cross(CentorBlock.transform.forward, nowobj.transform.forward)); break;
            }

         /*   if(nowobj.GetComponent<Shape>().shapetype == Shape.ShapeType.triatangle)
            {
                Debug.Log(Vector3.Cross(CentorBlock.transform.forward, nowobj.transform.forward));
                Debug.Log(CentorBlock.transform.forward + " " + nowobj.transform.forward);
                Debug.Log(CentorBlock.name + " " + nowobj.name);

             //   Debug.Log(Vector3.Cross(CentorBlock.transform.forward, CentorBlock.transform.forward));
            }*/

        }

        //TODO:answerblocksが見えないようにする
        foreach(GameObject nowobj in AnswerBlocks)
        {
            //解答を非表示にする
            MeshRenderer[] meshs = GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer nowm in meshs)
            {
                nowm.enabled = false;
            }

        }

        this.enabled = false;


    }

    // Update is called once per frame
    void Update()
    {
        /*Vector3 answery = Vector3.Cross(CentorBlock.transform.forward, answersblock.transform.forward);
        Vector3 playery = Vector3.Cross(playerscentor.transform.forward, playersblock.transform.forward);
        float y = Mathf.Abs((playery.y - answery.y));
        Debug.Log(answery + " " + playery + " " + y);*/

        //処理が重いのでプレイヤーがステージ４に入っている時だけ回す

        time += Time.deltaTime;
        if(time > 1)
        {
            FindAnsGroup();　//センターブロックと同じ構成要素のグループのみ評価する
            foreach (List<GameObject> g in CentorGroups)
            {
                CheckPoint(g);
            }

            //Debug.Log(ShapePoint);

            if(ShapePoint <= NierPoint)
            {
                if(is_test)Debug.Log("似ています");
                //this.enabled = false;
                success.SuccessMethod();
                this.enabled = false;
            }
            else
            {
                if (is_test) Debug.Log("似ていません");
            }

            time = 0;
        }

    }

    //センターブロックが所属しているブロックのリスト（グループ）を見つけて、shapeのlistをつくる。
    //グループのブロック数が、アンサーと違う場合は評価しない。(100000にされる)
    //府ループの形を更新するときだけ見ればいい

    public void FindAnsGroup()
    {
        //最大値を代入。これが小さいほど正解ブロックの形に近い
        ShapePoint = 2;

        //模範ブロックと同じ構成のグループだけ採点する
        List<GroupList> blockGroups = BlockGroupManager.Instance.GetGroupList();
        CentorGroups.Clear();

        foreach (GroupList nowgroup in blockGroups)
        {
            int square = 0;
            int rectangle = 0;
            int triangle = 0;

            //グループに含まれているブロックの数を種類ごとに数える
            foreach (Block nowblock in nowgroup.blockList)
            {
                switch (nowblock.gameObject.GetComponent<Shape>().shapetype)
                {
                    case Shape.ShapeType.square: square++; break;
                    case Shape.ShapeType.rectangle: rectangle++; break;
                    case Shape.ShapeType.triangle: triangle++; break;
                }
            }

            //正解と同じ数だった場合は審査する
            if(square == AnswerSquareBlocksPos.Count && rectangle == AnswerRectangleBlocksPos.Count && triangle == AnswerTriangleBlocksPos.Count)
            {
                List<GameObject> centorGroup = GLToGGList(nowgroup);

                if(centorGroup.Contains(CentorBlock) == false)
                {
                    CentorGroups.Add(centorGroup);
                }
            }

        }
    }
    //GroupListとList<GameObject>に変換する
    private List<GameObject> GLToGGList(GroupList groupList)
    {
        List<GameObject> g_list = new List<GameObject>();
        
        foreach (Block nowblock in groupList.blockList)
        {
            g_list.Add(nowblock.gameObject);
        }

        return g_list;
    }

    //グループ内にセンターと同じ形のブロックが複数ある場合、もっとも解答に近いものをセンターとする
    private void CheckPoint(List<GameObject> PlayerBlocks)
    {


        foreach (GameObject centorBlock in PlayerBlocks)
        {
            if(centorBlock.GetComponent<Shape>().shapetype == CentorType)
            {

                //ここでセンターから比較した値を取得する
                List<Vector3> squarePoslist = new List<Vector3>();
                List<Vector3> rectanglePoslist = new List<Vector3>();
                List<Vector3> trianglePoslist = new List<Vector3>();
                List<Vector3> squareAnglelist = new List<Vector3>();
                List<Vector3> rectangleAnglelist = new List<Vector3>();
                List<Vector3> triangleAnglelist = new List<Vector3>();

                foreach (GameObject nowobj in PlayerBlocks)
                {
                    switch (nowobj.GetComponent<Shape>().shapetype)
                    {
                        case Shape.ShapeType.square: squarePoslist.Add(nowobj.transform.position - centorBlock.transform.position); break;
                        case Shape.ShapeType.rectangle: rectanglePoslist.Add(nowobj.transform.position - centorBlock.transform.position); break;
                        case Shape.ShapeType.triangle: trianglePoslist.Add(nowobj.transform.position - centorBlock.transform.position); break;
                    }
                }

                foreach (GameObject nowobj in PlayerBlocks)
                {
                    switch (nowobj.GetComponent<Shape>().shapetype)
                    {
                        case Shape.ShapeType.square: squareAnglelist.Add(Vector3.Cross(centorBlock.transform.forward, nowobj.transform.forward)); break;
                        case Shape.ShapeType.rectangle: rectangleAnglelist.Add(Vector3.Cross(centorBlock.transform.forward, nowobj.transform.forward)); break;
                        case Shape.ShapeType.triangle: triangleAnglelist.Add(Vector3.Cross(centorBlock.transform.forward, nowobj.transform.forward)); break;
                    }
                }

                //ここの時点でわかってるのはブロックリストとセンターだけ、最適な組み合わせの部分を評価してもらう
                float squarePoint = CalcSquarePoint(squarePoslist, squareAnglelist);
                float rectanglePoint = CalcRectanglePoint(rectanglePoslist, rectangleAnglelist);
                float trianglePoint = CalcTrianglePoint(trianglePoslist, triangleAnglelist); 
                

                //Debug.Log("SquarePoint" + squarePoint + "RectanglePoint" + rectanglePoint + "TrianglePoint" + trianglePoint);
                float SumP = squarePoint + rectanglePoint + trianglePoint;

                if (SumP < ShapePoint)
                {
                    SquarePoint = squarePoint;
                    RectanglePoint = rectanglePoint;
                    TrianglePoint = trianglePoint;
                    ShapePoint = SumP;
                }
            }
        }
    }

    //もっとも正解ブロックに近い組み合わせの点数を返す
    private float CalcSquarePoint(List<Vector3> squarePoslist,List<Vector3> squareAnglelist)
    {
        //正方形がひとつもない場合は満点で返す
        if (squarePoslist.Count() == 0) { return 0; }

        float PosMax = (priorityPosX + priorityPosY + priorityPosX) * (squarePoslist.Count) * PositionDiffMax;
        float AngleMax = (priorityAngleX*0.5f + priorityAngleY*0.5f + priorityAngleZ*0.5f) * (squareAnglelist.Count);
        float sum = 2;

        foreach (int[] item in GetPermutation(Enumerable.Range(0, squarePoslist.Count)))
        {
            float PosP = 0;
            float AngleP = 0;

            for (int i = 0; i < item.Count(); i++)
            {
                //距離のポイントを計算
                PosP += Mathf.Abs(squarePoslist[item[i]].x - AnswerSquareBlocksPos[i].x) * priorityPosX;
                PosP += Mathf.Abs(squarePoslist[item[i]].y - AnswerSquareBlocksPos[i].y) * priorityPosY;
                PosP += Mathf.Abs(squarePoslist[item[i]].z - AnswerSquareBlocksPos[i].z) * priorityPosZ;

                //角度のポイントを計算 xは90ごと　yは90度ごと　zは90度ごと
                //AngleP += Mathf.Abs((squareAnglelist[item[i]].x - AnswerSquareBlocksAngle[i].x)) * priorityAngleX;
                AngleP += (0.5f - Mathf.Abs(0.5f - Mathf.Abs(squareAnglelist[item[i]].x - AnswerSquareBlocksAngle[i].x))) * priorityAngleX;
                //AngleP += Mathf.Abs((squareAnglelist[item[i]].y - AnswerSquareBlocksAngle[i].y)) * priorityAngleY;
                AngleP += (0.5f - Mathf.Abs(0.5f - Mathf.Abs(squareAnglelist[item[i]].y - AnswerSquareBlocksAngle[i].y))) * priorityAngleY;
                //AngleP += Mathf.Abs((squareAnglelist[item[i]].z - AnswerSquareBlocksAngle[i].z)) * priorityAngleZ;
                AngleP += (0.5f - Mathf.Abs(0.5f - Mathf.Abs(squareAnglelist[item[i]].z - AnswerSquareBlocksAngle[i].z))) * priorityAngleZ;
            }

            PosP = PosP / PosMax;
            AngleP = AngleP / AngleMax;

            if (PosP * priorityPos + AngleP * (1 - priorityPos) < sum)
            {
                sum = PosP * priorityPos + AngleP * (1 - priorityPos);
            }
        }
        return sum;
    }

    private float CalcRectanglePoint(List<Vector3> rectanglePoslist, List<Vector3> rectangleAnglelist)
    {
        //長方形がひとつもない場合は満点で返す
        if (rectanglePoslist.Count() == 0) { return 0; }

        float PosMax = (priorityPosX + priorityPosY + priorityPosX) * (rectanglePoslist.Count) * PositionDiffMax;
        float AngleMax = (priorityAngleX + priorityAngleY * 0.5f + priorityAngleZ) * (rectangleAnglelist.Count);
        float sum = 2;

        foreach (int[] item in GetPermutation(Enumerable.Range(0, rectanglePoslist.Count)))
        {
            float PosP = 0;
            float AngleP = 0;

            for(int i=0; i<item.Count(); i++)
            {
                //距離のポイントを計算
                PosP += Mathf.Abs(rectanglePoslist[item[i]].x - AnswerRectangleBlocksPos[i].x) * priorityPosX;
                PosP += Mathf.Abs(rectanglePoslist[item[i]].y - AnswerRectangleBlocksPos[i].y) * priorityPosY;
                PosP += Mathf.Abs(rectanglePoslist[item[i]].z - AnswerRectangleBlocksPos[i].z) * priorityPosZ;

                //角度のポイントを計算 xは180ごと　yは90度ごと　zは180度ごと
                AngleP += Mathf.Abs((rectangleAnglelist[item[i]].x - AnswerRectangleBlocksAngle[i].x)) * priorityAngleX;
                AngleP += (0.5f - Mathf.Abs(0.5f - Mathf.Abs(rectangleAnglelist[item[i]].y - AnswerRectangleBlocksAngle[i].y))) * priorityAngleY;
                AngleP += Mathf.Abs((rectangleAnglelist[item[i]].z - AnswerRectangleBlocksAngle[i].z)) * priorityAngleZ;
            }

            Debug.Log("Xの評価は" + rectangleAnglelist[item[0]].x + " " + AnswerRectangleBlocksAngle[0].x);
            Debug.Log("Yの評価は" + rectangleAnglelist[item[0]].y + " " + AnswerRectangleBlocksAngle[0].y);
            Debug.Log("Zの評価は" + rectangleAnglelist[item[0]].z + " " + AnswerRectangleBlocksAngle[0].z);

            PosP = PosP / PosMax;
            AngleP = AngleP / AngleMax;

            if (PosP * priorityPos + AngleP * (1 - priorityPos) < sum)
            {
                sum = PosP * priorityPos + AngleP * (1 - priorityPos);
            }
        }

        return sum;
    }

    private float CalcTrianglePoint(List<Vector3> trianglePoslist, List<Vector3> triangleAnglelist)
    {
        //三角形がひとつもない場合は満点で返す
        if (trianglePoslist.Count() == 0) { return 0; }

        float PosMax = (priorityPosX + priorityPosY + priorityPosX) * (trianglePoslist.Count) * PositionDiffMax;
        float AngleMax = (priorityAngleX + priorityAngleY + priorityAngleZ) * (triangleAnglelist.Count);
        float sum = 2;

        foreach (int[] item in GetPermutation(Enumerable.Range(0, trianglePoslist.Count)))
        {
            float PosP = 0;
            float AngleP = 0;

            for (int i = 0; i < item.Count(); i++)
            {
                //距離のポイントを計算
                PosP += Mathf.Abs(trianglePoslist[item[i]].x - AnswerTriangleBlocksPos[i].x) * priorityPosX;
                PosP += Mathf.Abs(trianglePoslist[item[i]].y - AnswerTriangleBlocksPos[i].y) * priorityPosY;
                PosP += Mathf.Abs(trianglePoslist[item[i]].z - AnswerTriangleBlocksPos[i].z) * priorityPosZ;

                //角度のポイントを計算 全部180度ごと
                AngleP += Mathf.Abs((triangleAnglelist[item[i]].x - AnswerTriangleBlocksAngle[i].x)) * priorityAngleX;
                AngleP += Mathf.Abs((triangleAnglelist[item[i]].y - AnswerTriangleBlocksAngle[i].y)) * priorityAngleY;
                AngleP += Mathf.Abs((triangleAnglelist[item[i]].z - AnswerTriangleBlocksAngle[i].z)) * priorityAngleZ;

          //      Debug.Log(triangleAnglelist[item[i]] + "  " + AnswerTriangleBlocksAngle[i]);
            }

       

            PosP = PosP / PosMax;
            AngleP = AngleP / AngleMax;

            //Debug.Log("ポジションポイントは " + PosP + "  角度ポイントは " + AngleP );

            if (PosP * priorityPos + AngleP * (1 - priorityPos) < sum)
            {
                sum = PosP * priorityPos + AngleP * (1 - priorityPos);
            }

        }

        return sum;
    }

    public static IEnumerable<T[]> GetPermutation<T>(IEnumerable<T> collection)
    {
        // 未確定要素が一個のみ
        if (collection.Count() == 1)
        {
            yield return new T[] { collection.First() };
        }

        foreach (var item in collection)
        {
            var selected = new T[] { item };
            var unused = collection.Except(selected);

            // 確定した要素以外の組み合わせを再帰で取り出し連結
            foreach (var rightside in GetPermutation(unused))
            {
                yield return selected.Concat(rightside).ToArray();
            }
        }
    }
}
