using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeDebug : MonoBehaviour
{
    //このスクリプトでしたいこと
    //特定のブロックを登録、召喚する。
    public List<GameObject> testBlocks;
    public Vector3 pos;

    // Start is called before the first frame update
    void Start()
    {
        //ブロックグループマネージャーに登録する.これはブロックスクリプトがついていると勝手に登録される
        foreach(GameObject nowobj in testBlocks)
        {
            GameObject Obj = Instantiate(nowobj, pos, Quaternion.identity);
        }
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
