﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;

public class WriteCSV : MonoBehaviour
{
    public string csvFilename;
    private StreamWriter writer;
    //public GameObject pupil;
    [SerializeField]
    private GameObject p_maasureWhite, p_measureRed, p_editWhite, p_edit_Red;
    // Start is called before the first frame update 
    void Start()
    {
        writer = new StreamWriter(csvFilename, false, Encoding.UTF8);
        writer.WriteLine($"t,posx,posy,posz");
    }

    // Update is called once per frame 
    void Update()
    {
        float t = Time.time;
        //float d1 = 1f;
        //float d2 = 2f;
        //float a1 = 3f;
        //writer.WriteLine($"{t},{d1},{d2},{a1}");
        writer.WriteLine($"{t},{p_maasureWhite.transform.position.x},{p_maasureWhite.transform.position.y},{p_maasureWhite.transform.position.z},{p_maasureWhite.transform.rotation.eulerAngles.x},{p_maasureWhite.transform.rotation.eulerAngles.y},{p_maasureWhite.transform.rotation.eulerAngles.y}," +
                        $"{p_measureRed.transform.position.x},{p_measureRed.transform.position.z},{p_measureRed.transform.position.z},{p_measureRed.transform.rotation.eulerAngles.x},{p_measureRed.transform.rotation.eulerAngles.y},{p_measureRed.transform.rotation.eulerAngles.z}," +
                        $"{p_editWhite.transform.position.x},{p_editWhite.transform.position.y},{p_editWhite.transform.position.z},{p_editWhite.transform.rotation.eulerAngles.x},{p_editWhite.transform.rotation.y},{p_editWhite.transform.rotation.eulerAngles.z}," +
                        $"{p_edit_Red.transform.position.x},{p_edit_Red.transform.position.y},{p_edit_Red.transform.position.z},{p_edit_Red.transform.rotation.eulerAngles.x},{p_edit_Red.transform.rotation.eulerAngles.y},{p_edit_Red.transform.rotation.eulerAngles.z}");
    }

    private void OnDestroy()
    {
        writer.Close();
    }
}