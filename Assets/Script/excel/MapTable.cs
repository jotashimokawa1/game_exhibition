﻿using System.Collections.Generic;
using UnityEngine;

public enum eMapObject
{
    None,
    Wall,
    Wall1,
    Wall2,
    Wall3,
    WallLine,
    TrapLine,
    CubeLine,
    Enemy00,
    Fixed00,
};



/// <summary>
/// MapTable (Sample)
/// </summary>
//public class MapTable : ScriptableObject
    public class MapTable : MonoBehaviour
{
    [SerializeField]
    private GameObject p_maasureWhite, p_measureRed, p_editWhite, p_edit_Red;

    public static Transform measured_white, measured_red, edit_white, edit_red;

    private void Start()
    {
        measured_white = p_maasureWhite.GetComponent<Transform>();
        measured_red = p_measureRed.GetComponent<Transform>();
        edit_white = p_editWhite.GetComponent<Transform>();
        edit_red = p_edit_Red.GetComponent<Transform>();
        
    }
    ///<summary>
    /// Table Row
    ///</summary>
    [System.Serializable]
    public class Row
    {
        public int          ID;
        public eMapObject   Type;
        public int          StageNo;
        public Vector3      pos1;
        public Quaternion   rot1;
        public Vector3 pos2;
        public Quaternion rot2;
        public Vector3 pos3;
        public Quaternion rot3;
        public Vector3 pos4;
        public Quaternion rot4;
    };

    /// <summary>
    /// テーブルデータ
    /// </summary>
    public static List<Row> Rows = new List<Row>()
    {
        /*
        new Row() {ID = 1, Type = eMapObject.Wall,     StageNo = 1, pos = measured_white.transform.position, rot = measured_white.transform.rotation},
        new Row() {ID = 2, Type = eMapObject.WallLine, StageNo = 1, pos = measured_red.transform.position, rot = measured_red.transform.rotation},
        new Row() {ID = 1, Type = eMapObject.Wall,     StageNo = 1, pos = edit_white.transform.position, rot = edit_white.transform.rotation },
        new Row() {ID = 2, Type = eMapObject.WallLine, StageNo = 1, pos = edit_red.transform.position, rot = edit_red.transform.rotation }
        */
        new Row() {ID = 1, Type = eMapObject.Wall,     StageNo = 1,  pos1 = new Vector3(0,0,0), rot1 = new Quaternion(0,0,0,1),pos2 = new Vector3(0,0,0), rot2 = new Quaternion(0,0,0,1),pos3 = new Vector3(0,0,0), rot3 = new Quaternion(0,0,0,1),pos4 = new Vector3(0,0,0), rot4 = new Quaternion(0,0,0,1)}
        
    };

    private void Update()
    {
        Rows[0].pos1 = measured_white.position;
        Rows[0].rot1 = measured_white.rotation;
        Rows[0].pos2 = measured_red.position;
        Rows[0].rot2 = measured_red.rotation;
        Rows[0].pos3 = edit_white.position;
        Rows[0].rot3 = edit_white.rotation;
        Rows[0].pos4 = edit_red.position;
        Rows[0].rot4 = edit_red.rotation;
    }
}
