using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WriteAndErase : MonoBehaviour
{

    [SerializeField] private TsumikiManager tsumikiManager;
    
    public void TargetFound()
    {
        tsumikiManager.findTarget(this.gameObject);

    }
    public void NormalFind()
    {
        tsumikiManager.NormalFind(this.gameObject);
    }

    public void TargetLost()
    {
        tsumikiManager.lostTarget(this.gameObject);


    }
    
}
