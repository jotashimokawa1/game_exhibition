using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantClear : MonoBehaviour
{
    public GameObject clearCanpas;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            clearCanpas.SetActive(true);

        }
    }
}
