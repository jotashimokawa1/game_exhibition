using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckSimilarity_J : MonoBehaviour
{
    
    public List<GameObject> m_original_check4, m_original_check5;
    public List<GameObject> m_replica_check;

    [SerializeField]
    private float m_threshold_distance = 1, m_threshold_angle = 30;
    [SerializeField]
    private float m_timer = 3f;
    
    [SerializeField]
    private OpenDoor m_door;
    [SerializeField]
    private bool m_isDoorOpen;

    //ロケットを発射する変数
    [SerializeField] private Rocket m_rocket;
    [SerializeField]
    private bool m_isRocketFlew;

    //タイマーに終了を通知
    public timer tim;

    private void Start()
    {
        m_isDoorOpen = false;
        m_isRocketFlew = false;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if(StageManager.Instance.CurrentCheckPoint == 4 && m_isDoorOpen == false)
        {
            if (BlockShapeSmilarity(m_original_check4, m_replica_check))
            {
                naviText.Instance.SetNaviText("次のチェックポイントに向かおう");
                m_door.SuccessMethod();
                m_isDoorOpen = true;
            }
        }

        if (StageManager.Instance.CurrentCheckPoint == 5 && m_isRocketFlew == false)
        {
            if (BlockShapeSmilarity(m_original_check5, m_replica_check))
            {
                naviText.Instance.SetNaviText("");
                m_rocket.SuccessMethod();
                m_isRocketFlew = true;

                //timerを止める
                tim.TimerStop();
            }
        }
        /* int size = 100;
         Debug.DrawRay(m_original_check4[1].transform.position ,m_original_check4[1].transform.forward*size, Color.red);
         Debug.DrawRay(m_original_check4[1].transform.position, m_original_check4[1].transform.right * size, Color.blue);
         Debug.DrawRay(m_original_check4[1].transform.position, m_original_check4[1].transform.up * size, Color.green);

         Debug.DrawRay(m_replica_check[1].transform.position, m_original_check4[1].transform.forward * size, Color.yellow);
         Debug.DrawRay(m_replica_check[1].transform.position, m_original_check4[1].transform.right * size, Color.blue);
         Debug.DrawRay(m_replica_check[1].transform.position, m_original_check4[1].transform.up * size, Color.green);
        */

    }

    private bool BlockShapeSmilarity(List<GameObject> original, List<GameObject> replica)
    {
        float distance = 0;
        float  angleScore = 0;
        if (original.Count == 2 && replica.Count == 2)
        {
            //距離を比べる
            float magnitude_original = (original[0].transform.position - original[1].transform.position).magnitude;
            float magnitude_target = (replica[0].transform.position - replica[1].transform.position).magnitude;
            distance = Mathf.Abs(magnitude_original - magnitude_target);
            

            //相対角度を調べる
            Vector2 angle_original = angleDif(original[0], original[1]);
            Vector2 angle_replica = angleDif(replica[0], replica[1]);
            angleScore = (angle_original - angle_replica).magnitude; //例えば，(30,10)ならスコアは31.6．体感30以下くらいで良い感じ．


        }
        else
        {
            Debug.Log("2個同士以外は非対応");
            return false;
        }

        //結果を帰す
        if (distance <= m_threshold_distance && angleScore <= m_threshold_angle)
        {
            //距離と角度が閾値以下なら成功
            return true;
        }
        else
        {
           // Debug.Log("dist:" + distance + " angle:" + angleScore);
            return false;
        }
    }


    //RightベクトルとUpベクトルの角度の差を帰す
    private Vector2 angleDif(GameObject baseObj, GameObject targetObj)
    {
        Transform baseTrans = baseObj.transform;
        Transform targetTrans = targetObj.transform;
        //rotation(0,0,0)の時，y方向に長い直方体
        //right，正方形の方
        float right_angle = Vector3.Angle(baseTrans.transform.right, targetTrans.transform.right);
        right_angle = right_angle % 90;
        if (right_angle > 45) { right_angle = 90 - right_angle; }

        float up_angle = Vector3.Angle(baseTrans.transform.up, targetTrans.transform.up);
        //upアングルを形状に合わせて変更（長方形）
        if (up_angle > 90)
        {
            up_angle = 180 - up_angle;
        }
        
        
        Vector2 right_up_angle = new Vector2(right_angle, up_angle);
  
        return right_up_angle;
    }
 

   }
