using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TsumikiManager : MonoBehaviour
{

    public List<GameObject> stage;
    public List<GameObject> objects;

    public GameObject BaseStageMarker;
    public List<GameObject> RecognitionBlocksDB;
    public List<GameObject> InGameBlockDB;
    [SerializeField, ReadOnly]
    private List<OriginalBlock> OriginalBlock_InGameBlocks;

    //通電ブロックにするためのオブジェクトを保存
    public GameObject ElecCollider;
    public Material yellow, green;
    [SerializeField] private float elecColliderSize = 1.3f;

    //ブロックを認識したときと、認識が外れたときに音が鳴る
    [SerializeField] private AudioSource PlayerAudioSource;
    [SerializeField] private AudioClip FindTargetSound;
    [SerializeField] private AudioClip LostTargetSound;

 

    private void Update()
    {

        int player_stage = StageManager.Instance.CurrentCheckPoint;
       
        //ステージの距離を取る
        Vector3 distance3 = stage[player_stage].transform.position - BaseStageMarker.transform.position;

        for (int j = 0; j < InGameBlockDB.Count; j++)
        {
            
            //複製が消えてないブロックは移動
            if(OriginalBlock_InGameBlocks[j].myKiz != null)
            {
                objects[j].transform.position = InGameBlockDB[j].transform.position + distance3;
                objects[j].transform.rotation = InGameBlockDB[j].transform.rotation;
            }
        }

    }

    /// <summary>
    /// 新しいターゲットを見つけた時に一度だけ呼ばれる処理
    /// </summary>
    /// <param name="recognizedBlock"></param>
    public void findTarget(GameObject recognizedBlock)
    {
        //「認識している」データベースに登録
        RecognitionBlocksDB.Add(recognizedBlock);
        //「認識したことがある」データベースに登録
        InGameBlockDB.Add(recognizedBlock);
        //ステージに複製する
        ReplicateBlock(recognizedBlock);
    }
    /// <summary>
    /// 複製する処理
    /// </summary>
    /// <param name="recognizedBlock"></param>
    private void ReplicateBlock(GameObject recognizedBlock)
    {
        //複製を生成(ステージ数だけ）

        GameObject block = Instantiate(recognizedBlock, recognizedBlock.transform.position + (stage[0].transform.position - BaseStageMarker.transform.position), recognizedBlock.transform.rotation);
        GameObject.Destroy(block.GetComponent<TsumikiDamper>());
        GameObject.Destroy(block.GetComponent<WriteAndErase>());
        GameObject.Destroy(block.GetComponent<OriginalBlock>());

        //複製を元のブロックに紐づける
        recognizedBlock.GetComponent<OriginalBlock>().myKiz = block;

        //OriginalBlockリストに追加
        OriginalBlock_InGameBlocks.Add(recognizedBlock.GetComponent<OriginalBlock>());

        //オブジェクトが物理的に動かないようにする
        Rigidbody rb = block.GetComponent<Rigidbody>();
        rb.isKinematic = true;

        //カメラが寄ってこない4:waterレイヤーにする
        block.layer = 4;

        //---通電ブロックにするための処理
        block.AddComponent<Block>();
        Block b = block.GetComponent<Block>();
        // b.normal = this.green;
        b.yellow = this.yellow;

        /*  GameObject Obj = (GameObject)Instantiate(ElecCollider, this.transform.position, Quaternion.identity);
          Obj.transform.parent = block.transform;
          Obj.transform.localPosition = new Vector3(0, 0, 0);
          Obj.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);*/

        //電気を感知する範囲を設定
        GameObject Obj = Instantiate(recognizedBlock, recognizedBlock.transform.position + (stage[0].transform.position - BaseStageMarker.transform.position), recognizedBlock.transform.rotation);

        GameObject.Destroy(Obj.GetComponent<WriteAndErase>());
        GameObject.Destroy(Obj.GetComponent<Rigidbody>());
        GameObject.Destroy(Obj.GetComponent<OriginalBlock>());
        GameObject.Destroy(Obj.GetComponent<TsumikiDamper>());
        Obj.GetComponent<MeshRenderer>().enabled = false;
        //ボックスコリダーかメッシュコリダーをisTriggerにする
        if (Obj.TryGetComponent(out BoxCollider bcol)) { bcol.isTrigger = true; }
        if (Obj.TryGetComponent(out MeshCollider mcol)) { mcol.isTrigger = true; }


        Obj.transform.parent = block.transform;
        Obj.transform.localPosition = new Vector3(0, 0, 0);
        Obj.transform.localScale = new Vector3(elecColliderSize,elecColliderSize,elecColliderSize);
        //-------------------------------

        //形を記録するための処理
        block.AddComponent<Shape>();
        Shape b_shape = block.GetComponent<Shape>();
        b_shape.shapetype = CheckShape(recognizedBlock);

        objects.Add(block);

        Debug.Log("Find Target");

        //エリアの衝突判定の設定 (エリアは関係ないのblock,areaと衝突しない）
        foreach (GameObject nowblock in InGameBlockDB)
        {
            foreach (GameObject anotherBlock in InGameBlockDB)
            {
                if (nowblock != anotherBlock)
                {
                    Physics.IgnoreCollision(nowblock.GetComponent<OriginalBlock>().AreaCol, anotherBlock.GetComponent<Collider>(), true);
                    Physics.IgnoreCollision(nowblock.GetComponent<OriginalBlock>().AreaCol, anotherBlock.GetComponent<OriginalBlock>().AreaCol, true);
                }
            }
        }

    }
    public void NormalFind(GameObject recognizedBlock)
    {
        //DBに登録
        int findnum = RecognitionBlocksDB.IndexOf(recognizedBlock);
        if(findnum == -1)
        {
            //2回目の発見以降
            RecognitionBlocksDB.Add(recognizedBlock);
           if(OriginalBlock_InGameBlocks[InGameBlockDB.IndexOf(recognizedBlock)].myKiz == null)
            {
                //リセットボタンでブロックが消えているとき
                
                //オリジナル（カメラに移っている方）を見えるようにする
                recognizedBlock.GetComponent<MeshRenderer>().enabled = true;
                recognizedBlock.GetComponent<OriginalBlock>().AreaCol.GetComponent<MeshRenderer>().enabled = true;
                //複製する
                ReplicateBlock(recognizedBlock);
            }
        }
        else
        {
            //1回目の発見
            //処理なし
        }

        //ブロックを認識したときに音がなる
        Debug.Log("ブロックをみつけました");
        PlayerAudioSource.volume = 0.6f;
        PlayerAudioSource.pitch = 1f;
        PlayerAudioSource.PlayOneShot(FindTargetSound);

        //認識したブロックを表示する
       /* BoxCollider boxcollider = recognizedBlock.GetComponent<BoxCollider>();
        boxcollider.enabled = false;
        MeshRenderer meshrenderer = recognizedBlock.GetComponent<MeshRenderer>();
        meshrenderer.enabled = false;*/
    }

    public void lostTarget(GameObject UnRecognizedBlock)
    {
        //Listの認識から外れたオブジェクトの位置
        int lostnum = RecognitionBlocksDB.IndexOf(UnRecognizedBlock);
        //マネージャのデータベースから削除
        RecognitionBlocksDB.Remove(UnRecognizedBlock);

        //ステージマーカの外に出たのか，ただ認識が外れただけなのか
        //LostTarget_outside(UnRecognizedBlock, lostnum);
        LostTarget_inside(UnRecognizedBlock);

        //ブロックを見失ったときに音がなる
        Debug.Log("ブロックを見失いました");
        PlayerAudioSource.volume = 0.6f;
        PlayerAudioSource.pitch = 1f;
        PlayerAudioSource.PlayOneShot(LostTargetSound);

        //認識したブロックを非表示にする
     /*   BoxCollider boxcollider = UnRecognizedBlock.GetComponent<BoxCollider>();
        boxcollider.enabled = true;
        MeshRenderer meshrenderer = UnRecognizedBlock.GetComponent<MeshRenderer>();
        meshrenderer.enabled = true;*/
    }
    private void LostTarget_inside(GameObject UnRecognitionBlock)
    {
        //中で見失ったときの処理をここに書く

    }
    private void LostTarget_outside(GameObject UnRecognizedBlock, int lostnum)
    {
        //originalを見えなくする
        UnRecognizedBlock.GetComponent<MeshRenderer>().enabled = false;
        UnRecognizedBlock.GetComponent<BoxCollider>().enabled = false;

        

        //複製を削除

            if (lostnum != -1)
            {
                //複製したオブジェクトをdestroy
                Destroy(objects[lostnum]);
                //リストから削除
                objects.RemoveAt(lostnum);
            }
            else
            {
                //ない場合
               Debug.Log("error: 要素なし");

            }

        Debug.Log("Lost Target-場外");

    }

    public Shape.ShapeType CheckShape(GameObject block)
    {
        Shape.ShapeType type = Shape.ShapeType.square;
        Debug.Log(block.gameObject.transform.parent.name);

        switch (block.gameObject.transform.parent.name) 
        {
            case "RedModelTarget": type = Shape.ShapeType.rectangle; break; //ホントはrectangleだよ
            case "RedModelTarget2": type = Shape.ShapeType.rectangle; break;
            case "BoxModelTarget": type = Shape.ShapeType.square; break;
            case "TriangleModelTarget1": type = Shape.ShapeType.triangle; break;
            default: Debug.Log("生成されたブロックは、名前に登録されていません");break;
        }

        return type;
    }

    public void TestDestroyUnTrackingObj()
    {
        Debug.Log("*Reset!");
        foreach(GameObject obj in InGameBlockDB.Except(RecognitionBlocksDB))
        {
            OriginalBlock objOB = obj.GetComponent<OriginalBlock>();
            //一度認識しているが，今は認識していないオブジェクト
            Debug.Log(obj.name);
            //Model Target（カメラに移っている方　を見えなくする）
            obj.GetComponent<MeshRenderer>().enabled = false;
            objOB.AreaCol.GetComponent<MeshRenderer>().enabled = false;

            //クローンを削除する
            if (objOB.myKiz != null)
            {
                //リストから消す
                int num = objects.IndexOf(objOB.myKiz); //リスト内のオブジェクトの場所
                objects.RemoveAt(num);
                //削除
                Destroy(objOB.myKiz);
            }
            else
            {
                Debug.LogError(" 何かが起こってすでにキッズがない!");
            }


        }
        //メモ：元，を見えるようにする．クローンをもう一度出す． 
    }

   
}
