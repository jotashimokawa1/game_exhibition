using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tsumikiReset : MonoBehaviour
{
    [SerializeField]
    private bool m_DeleteUnTrackingBlock = false;

    public TsumikiManager m_TsumikiManager;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_DeleteUnTrackingBlock)
        {
            //認識していないブロックを削除
            m_TsumikiManager.TestDestroyUnTrackingObj();


            //初期化
            m_DeleteUnTrackingBlock = false;
        }
    }
}
