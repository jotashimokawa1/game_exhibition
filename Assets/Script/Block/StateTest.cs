using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateTest : MonoBehaviour
{


    public void ONE()
    {
        Debug.Log("ワンワン");
    }

    public void TWO()
    {
        Debug.Log("トゥートゥー");
    }

    public void BlockStacked()
    {
        Debug.Log("合体");
    }

    public void BlockSeparated()
    {
        Debug.Log("分かれた");
    }

    public void BlockPicked()
    {
        Debug.Log("持ち上げられた");
    }

    public void BlockPut()
    {
        Debug.Log("置かれた");
    }
}
