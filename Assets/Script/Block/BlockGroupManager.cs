using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BlockGroupManager : Singleton<BlockGroupManager>
{
    public List<Block> BlockList = new List<Block>();
    private List<Block> BlockListCopy;
    [SerializeField] private Block Dynamo;

    /*
    [System.SerializableAttribute]
    public class GroupList 
    {
        public List<Block> blockList = new List<Block>();

        public GroupList(List<Block> list)
        {
            blockList = list;
        }
    }*/

    [SerializeField]
    private List<GroupList> blockGroups = new List<GroupList>();

    public UnityEvent BlockStacked; //合体したとき
    public UnityEvent BlockSeparated; //分かれたとき

    public UnityEvent BlockPicked; //ブロックが持ち上げられているとき　（ブロックが動いているとき）
    public UnityEvent BlockPut; //ブロックが置かれたとき　（ブロックが止まっているとき）
    [SerializeField] private bool IsPicked = false; //変化時にイベントが呼び出される。pickedイベントから始まるので、初期値はfalseにする。
    public void SetIsPicked(bool new_IsPicked)
    {
        if(IsPicked == new_IsPicked)
        {
            Debug.Log("同じ状態がセットされています。変更するときだけセットしてください");
        }
        else
        {
            if(new_IsPicked == true)
            {
                BlockPicked.Invoke();
            }
            else
            {
                BlockPut.Invoke();
            }
        }
    }



    //ShapeChackerがまとまりを検知するために使う
    public List<GroupList> GetGroupList()
    {
        return blockGroups;
    }

    //新しく作られたブロックを記録する
    public void addBlocklist(Block block)
    {
        BlockList.Add(block);
    }

    public void removeBlockList(Block block)
    {
        BlockList.Remove(block);
    }

    //ブロックをまとまりのグループごとに分ける
    public void SortBlockGroups()
    {
        //グループの増減を感知するためにグループの数を記録
        int beforeGroupCount = blockGroups.Count;

        blockGroups.Clear();

        //ブロックリストのコピーを作成。これを振り分けていく。
        BlockListCopy = new List<Block>(BlockList);

        while(BlockListCopy.Count > 0)
        {
            //新しいグループを作成
            List<Block> blist;

            blist = MakeGroups(BlockListCopy[0]);

            GroupList group = new GroupList(blist);
            blockGroups.Add(group);
        }

        //グループの増減を比較
        int afterGroupCount = blockGroups.Count;
        if( afterGroupCount < beforeGroupCount)
        {
            //合体しているときのイベント
            BlockStacked.Invoke();
            
        }else if(afterGroupCount > beforeGroupCount)
        {
            //分かれたときときのイベント
            BlockSeparated.Invoke();
        }

        //通電
        giveElec();
    }

    private List<Block> MakeGroups(Block b)
    {
        //どこかのグループにすでに割り当てられている場合は何もせずに返す
        if (!BlockListCopy.Contains(b))
        {
            return null;
        }

        //自身をグループに追加
        List<Block> b_list = new List<Block>();
        b_list.Add(b);
        BlockListCopy.Remove(b);

        //接しているブロックの一覧を取得して、再帰的に呼び出し
        List<Block> b_connect_list = b.m_connectBlockList;
        foreach(Block nowConnectBlock in b_connect_list)
        {
            List<Block> b_addlist = MakeGroups(nowConnectBlock);

            //親のグループと合成
            if(b_addlist != null)
            {
                b_list.AddRange(b_addlist);
            }
        }

        //最後まで確認すれば終わり
        return b_list;
    }

    private void giveElec()
    {
        List<Block> dynamoGroup = null;
        
        //Block全ての通電状態をリセット
        foreach (GroupList nowlist in blockGroups)
        {
            foreach (Block nowblock in nowlist.blockList)
            {
                nowblock.electrified = false;

                //ダイナモが所属しているグループを検索
                if (nowblock == Dynamo)
                {
                    dynamoGroup = nowlist.blockList;
                }
            }
        }
        
        //グループ全体に通電する
        foreach(Block nowblock in dynamoGroup)
        {
            nowblock.electrified = true;
        }
    }
        
    
    


}
