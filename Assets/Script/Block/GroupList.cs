using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.SerializableAttribute]
public class GroupList
{
    public List<Block> blockList = new List<Block>();

    public GroupList(List<Block> list)
    {
        blockList = list;
    }
}
