using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine;

public class BlockState : Singleton<BlockState>
{
    //絶対に通知されるだけにする。ごちゃごちゃしてきた

    [SerializeField] GameObject stage; //今使われているステージをチェックポイントを通るたびに更新

    public List<Block> blocks; //別に要らないけど、デバッグのために置いてる
    public bool IsPicking = false;

    private int a=0;

    //ブロックに移動があるたびにチェックする
    public void CheckPicking()
    {
        bool afterpicking = false;
        blocks = BlockGroupManager.Instance.BlockList;

        foreach(Block nowBlock in blocks)
        {
            if(nowBlock.GetIsConnect() == false)
            {
                //ひとつでも浮いていれば、移動中と判断する
                afterpicking = true;
                //break;
                //Debug.Log(a + "番目のチェック " + nowBlock.gameObject.name + "は浮いています");
            }
        }

        if (IsPicking == false && afterpicking == true)
        {
            BlockGroupManager.Instance.BlockPicked.Invoke();
            //Debug.Log("pickされました");
            IsPicking = true;
        }else if (IsPicking == true && afterpicking == false)
        {
            BlockGroupManager.Instance.BlockPut.Invoke();
            //Debug.Log("putされました");
            IsPicking = false;
        }

        a++;

    }



}