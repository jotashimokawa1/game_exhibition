using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Elevator : MonoBehaviour
{
    Sequence sequence;

    Vector3[] path = 
    { 
          new Vector3(1370,-139,-1842),
          new Vector3(1370,440,-1842)
    };

    private void Start()
    {
        sequence = DOTween.Sequence();
        sequence.Append(transform.DOMove(new Vector3(1370, 438, -1842), 5f).SetDelay(2f))
            .SetLoops(-1, LoopType.Yoyo);
        sequence.Pause();
    }



    public void SwitchON()
    {
        sequence.Play();
        if (StageManager.Instance.CurrentCheckPoint == 3)
        {
            naviText.Instance.SetNaviText("エレベーターに乗ろう");
        }
    }

    public void SwitchOFF()
    {
        sequence.Pause();
    }

}
