using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{

    public Material yellow;
    protected Material normal;

    //帯電
    [SerializeField]protected bool m_electrified = false;

    //配下のブロックリスト
    public List<Block> m_connectBlockList = new List<Block>();

    public bool electrified
    {
        get { return m_electrified; }
        set { 
                m_electrified = value;
                CheckElec(value);
            }
    }

    //浮いているかどうかを判定
    [SerializeField]private bool IsConnect = false;
    private bool sendConnectInfo = false;

    public void Start()
    {
        normal = GetComponent<MeshRenderer>().material;
        BlockGroupManager.Instance.addBlocklist(GetComponent<Block>());
    }

    private void LateUpdate()
    {
        //全ての処理の後に行われるハズ
        if(sendConnectInfo == true)
        {
            BlockState.Instance.CheckPicking();
            sendConnectInfo = false;
        }
    }



    //配下のブロックに通電する
    public void getElec()
    {
        //自分と接しているブロックに通電する
        foreach(Block nowblock in m_connectBlockList)
        {
            if(nowblock.electrified == false)
            {
                //Debug.Log(this.name + "が" + nowblock.name + "に通電したよ");
                nowblock.electrified = true;
            }
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        other.TryGetComponent(out Block otherBlock);
        if (otherBlock != null)
        {
            //すでにリストに入っているかどうかを確認
           if(m_connectBlockList.Contains(otherBlock) == false)
            {
                m_connectBlockList.Add(otherBlock);
            }

            //電気物に接するとき、自分が電気物でなければ、自分にも通電する
            if (otherBlock.electrified == true)
            {
                electrified = true;
            }
        }
        BlockGroupManager.Instance.SortBlockGroups();   
    }


    public void OnTriggerExit(Collider other)
    {
        other.TryGetComponent(out Block otherBlock);
        if(otherBlock != null)
        {
            if (m_connectBlockList.Contains(otherBlock) == true)
            {
                m_connectBlockList.Remove(otherBlock);
            }
        }
        BlockGroupManager.Instance.SortBlockGroups();

        if(IsConnect == true)
        {
            IsConnect = false;
            sendConnectInfo = true;
           // BlockState.Instance.CheckPicking();
        }

    }

    public virtual void CheckElec(bool value)
    {
        if (value == true)
        {
            getElec();
            gameObject.GetComponent<MeshRenderer>().material = yellow;
        }
        else
        {
            gameObject.GetComponent<MeshRenderer>().material = normal;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(IsConnect == false)
        {
            IsConnect = true;
            sendConnectInfo = true;
           // BlockState.Instance.CheckPicking();
        }
    }

    public bool GetIsConnect() 
    {
        return IsConnect;
    }





}
