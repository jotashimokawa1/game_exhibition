using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Camera))]
public class ThirdPersonCamera : MonoBehaviour
{
    public GameObject primaryTarget;
    public GameObject secondaryTarget;
    public Vector2 primaryLockOnPosition = new Vector2(0, 0);
    public Vector2 secondaryLockOnPosition = new Vector2(0, .3f);
    public CameraMode cameraMode;
    public float distance = 4;
    public float height = 1.5f;
    public float delay = 2;
    [Range(0, 1)]
    public float maxVerticalRotationLimit = .9f;
    [Range(0, 1)]
    public float minVerticalRotationLimit = .9f;
    public LayerMask layerMask;
    public float castRadius = .6f;
    [Range(.001f, 1)]
    public float lockOnSpeed = .1f;
    public AnimationCurve shakeDecayCurve = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 0));
    public bool inputInThisScript = true;
    public float rotatespeed = 10f;

    private Ps5 playerActionsAsset;
    private InputAction camera_move;
    public bool isMouse = false;

    private void Awake()
    {
        playerActionsAsset = new Ps5();
        camera_move = playerActionsAsset.player.Camera;
        playerActionsAsset.player.Enable();
    }

    public enum CameraMode
    {
        Manual,
        Auto,
        LockOn
    }

    private Camera cam;
    private Transform orbitalCenter;
    private Vector2 primPosOffset;
    private Vector2 secPosOffse;
    private float fixedDistance;
    private float frustumHeight;
    private float frustumWidth;
    private float lastAspect;
    private float shakePower;
    private float shakeSpeed;
    private float shakeLifeTime;
    private float shakeAge;
    private bool shake = false;

    private void Start()
    {
        cam = GetComponent<Camera>();
        orbitalCenter = new GameObject().transform;

        SetFrustumSize();
        SetPositionOffset();
        orbitalCenter.position = primaryTarget.transform.position + Vector3.up * height;
        transform.position = orbitalCenter.position - orbitalCenter.forward * fixedDistance;
        transform.LookAt(orbitalCenter);
        transform.position = transform.position - transform.right * primPosOffset.x - transform.up * primPosOffset.y;
    }

    private void LateUpdate()
    {

        /*   if (Input.GetMouseButton(1))
           {
               if (inputInThisScript) Inputa();
           }*/

        if(isMouse == true)
        {
            if (Input.GetMouseButton(1))
            {
                if (inputInThisScript) Inputa();
            }
        }
        else
        {
            if (inputInThisScript) Inputa();
        }

        



        if (cam.aspect != lastAspect) SetFrustumSize();

        if (cameraMode == CameraMode.Auto) AutoCamera();
        else if (cameraMode == CameraMode.LockOn) LockOnCamera();

        ObstacleDetection();
        SetPositionOffset();
        MoveCamera();
        SetLastValues();
        if (shake) Shake();
    }

    private void Inputa()
    {

       // RotateCamera(UnityEngine.InputSystem.Mouse.current.delta.ReadValue() * rotatespeed);
        RotateCamera(playerActionsAsset.player.Camera.ReadValue<Vector2>()   * rotatespeed);
    }

    private void ObstacleDetection()
    {
        RaycastHit hitInfo;
        if (Physics.SphereCast(orbitalCenter.position, castRadius, transform.position - orbitalCenter.position, out hitInfo, distance, layerMask) ||
            Physics.Raycast(orbitalCenter.position, transform.position - orbitalCenter.position, out hitInfo, distance, layerMask))
        {
            fixedDistance = Mathf.Sqrt(primPosOffset.x * primPosOffset.x + primPosOffset.y * primPosOffset.y + hitInfo.distance * hitInfo.distance);
        }
        else
        {
            fixedDistance = distance;
        }
    }

    private void SetFrustumSize()
    {
        frustumHeight = 2 * Mathf.Tan(cam.fieldOfView * .5f * Mathf.Deg2Rad);
        frustumWidth = frustumHeight * cam.aspect;
    }

    private void SetPositionOffset()
    {
        primPosOffset.x = frustumWidth * fixedDistance * primaryLockOnPosition.x;
        primPosOffset.y = frustumHeight * fixedDistance * primaryLockOnPosition.y;
        secPosOffse.x = frustumWidth * fixedDistance * secondaryLockOnPosition.x - primaryLockOnPosition.x;
        secPosOffse.y = frustumHeight * fixedDistance * secondaryLockOnPosition.y - primaryLockOnPosition.x;
    }

    private void MoveCamera()
    {
        var camTgtPos = primaryTarget.transform.position + Vector3.up * height;
        var curVel = Vector3.zero;
        orbitalCenter.position = Vector3.SmoothDamp(orbitalCenter.position, camTgtPos, ref curVel, delay * Time.deltaTime);
        transform.position = orbitalCenter.position - orbitalCenter.forward * fixedDistance;
        transform.LookAt(orbitalCenter);
        transform.position = transform.position - transform.right * primPosOffset.x - transform.up * primPosOffset.y;
    }

    private void AutoCamera()
    {
        orbitalCenter.rotation = Quaternion.Lerp(orbitalCenter.rotation, Quaternion.LookRotation(primaryTarget.transform.forward, Vector3.up), lockOnSpeed);
    }

    private void LockOnCamera()
    {
        var tgtVecFromCam = transform.forward + transform.right * frustumWidth * (secondaryLockOnPosition.x - primaryLockOnPosition.x) + transform.up * frustumHeight * (secondaryLockOnPosition.y - primaryLockOnPosition.y);
        var secTgtDir = secondaryTarget.transform.position + Vector3.up * height - orbitalCenter.transform.position;
        var ratio = Mathf.Sqrt(secTgtDir.sqrMagnitude + tgtVecFromCam.sqrMagnitude);
        var tgtFwd = secTgtDir - transform.right * ratio * frustumWidth * (secondaryLockOnPosition.x - primaryLockOnPosition.x) - transform.up * ratio * frustumHeight * (secondaryLockOnPosition.y - primaryLockOnPosition.y);
        orbitalCenter.rotation = Quaternion.Slerp(orbitalCenter.rotation, Quaternion.LookRotation(tgtFwd, Vector3.up), lockOnSpeed);
    }

    private void SetLastValues()
    {
        lastAspect = cam.aspect;
    }

    private void Shake()
    {
        shakeAge += Time.deltaTime;
        var time = Time.time * shakeSpeed;
        var noiseX = (Mathf.PerlinNoise(time, time) - .5f) * shakePower * shakeDecayCurve.Evaluate(shakeAge / shakeLifeTime);
        var noiseY = (Mathf.PerlinNoise(time, time + 128) - .5f) * shakePower * shakePower * shakeDecayCurve.Evaluate(shakeAge / shakeLifeTime);
        transform.rotation = Quaternion.Euler(noiseX, noiseY, 0) * transform.rotation;
    }

    IEnumerator SetShake(float waitTime)
    {
        shake = true;
        yield return new WaitForSeconds(waitTime);
        shake = false;
    }


    public void RotateCamera(Vector2 input)
    {
        if (cameraMode != CameraMode.Manual) return;
        input.y *= -1;
        var isExMaxLim = input.y < 0 && transform.forward.y > minVerticalRotationLimit;
        var isExMinLim = input.y > 0 && transform.forward.y < -maxVerticalRotationLimit;
        if (isExMaxLim || isExMinLim) input.y = 0;
        input *= Time.deltaTime;
        orbitalCenter.RotateAround(orbitalCenter.position, Vector3.up, input.x);
        orbitalCenter.RotateAround(orbitalCenter.position, transform.right, input.y);
    }

    public void ShakeCamera(float power, float speed, float shakeTime)
    {
        shakePower = power;
        shakeSpeed = speed;
        shakeLifeTime = shakeTime;
        shakeAge = 0;
        IEnumerator setShale = SetShake(shakeTime);
        StartCoroutine(setShale);
    }

    public void SetTarget(GameObject target)
    {
        secondaryTarget = target;
    }
}