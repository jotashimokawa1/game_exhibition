using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TsumikiCamera : MonoBehaviour
{
    [SerializeField] GameObject ARcamera;
    private Camera camera;
    [SerializeField] GameObject Gamecamera;
    [SerializeField] GameObject stageplane;
    [SerializeField] GameObject BaseStageMarker;
    private Vector3 difPos;
    private bool Is_Active = false;
    private bool Is_Return = false;

    private float returntime = 0;
    private Vector3 set_camerapos;
    private Quaternion set_cameraqua;

    private Sequence EnterDoTween;
    private Sequence ExitDoTween;

    // Start is called before the first frame update
    void Start()
    {
        difPos = stageplane.transform.position - BaseStageMarker.transform.position;
        camera = Gamecamera.GetComponent<Camera>();
        camera.depth = -2;
    }

    // Update is called once per frame
    void Update()
    {
        //ARカメラと同じ動きをする
        if (Is_Active)
        {
            Gamecamera.transform.position = ARcamera.transform.position + difPos;
            Gamecamera.transform.rotation = ARcamera.transform.rotation;

            set_camerapos = Gamecamera.transform.position;
            set_cameraqua = Gamecamera.transform.rotation;
        }
        
        //メインカメラにスムーズに戻す処理
        if (Is_Return)
        {
            //２秒かけて戻る場合
            returntime += Time.deltaTime / 2;
            Gamecamera.transform.rotation = Quaternion.Lerp(set_cameraqua, Camera.main.transform.rotation,returntime);
            Gamecamera.transform.position = Vector3.Lerp(set_camerapos, Camera.main.transform.position,returntime);

            if(returntime >= 1)
            {
                Is_Return = false;
                camera.depth = -2;
                returntime = 0;
                Debug.Log("カメラ切り替え");
            }
        }
    }

   　void OnCollisionEnter(Collision collision)
    {


        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("入った");

            //離れている最中の場合、動きを切り替える
            if (Is_Return)
            {
                Is_Return = false;
                returntime = 0;
                ExitDoTween.Kill();
            }            
            

            Gamecamera.transform.position = Camera.main.transform.position;
            Gamecamera.transform.rotation = Camera.main.transform.rotation;
            camera.depth = 0;

            camera.fieldOfView = Camera.main.fieldOfView;

            EnterDoTween = DOTween.Sequence();

            //スムーズにツミキカメラに切り替える処理
            EnterDoTween.Append(
                Gamecamera.transform.DOMove(ARcamera.transform.position + difPos, 2)

            );

            EnterDoTween.Join(
                Gamecamera.transform.DORotateQuaternion(ARcamera.transform.rotation, 2)
            );

            EnterDoTween.Join(
                DOVirtual.Float(Camera.main.fieldOfView, 36, 2, value =>
                {
                    camera.fieldOfView = value;
                })
            );

            EnterDoTween.OnUpdate(() => {
                set_camerapos = Gamecamera.transform.position;
                set_cameraqua = Gamecamera.transform.rotation;
            });

            EnterDoTween.OnComplete(()=> { Is_Active = true; });

        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("でた");
            Is_Active = false;
            Is_Return = true;

            EnterDoTween.Kill();

            ExitDoTween = DOTween.Sequence();
            ExitDoTween.Append(
                DOVirtual.Float(camera.fieldOfView, Camera.main.fieldOfView, 2, value =>
                {
                    camera.fieldOfView = value;
                })
            );
        }
    }

    //壁に埋まったらどうする？

}
