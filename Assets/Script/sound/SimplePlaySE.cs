using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlaySE : MonoBehaviour
{
    [SerializeField] GameObject Player;
    [SerializeField] AudioSource audioSource;
    [SerializeField] List<AudioClip> audioSourceList = new List<AudioClip>();

    public void PlaySE(int num)
    {
        AudioClip audioclip;
        switch (num)
        {
            case 0: audioclip = audioSourceList[0]; break;
            case 1: audioclip = audioSourceList[1]; break;
            case 2: audioclip = audioSourceList[2]; break;
            case 3: audioclip = audioSourceList[3]; break;
            default: audioclip = audioSourceList[0]; break;

        }
        float Volume = 0.6f;
        float pitch = 1;
        audioSource.volume = Volume;
        audioSource.pitch = pitch;

        audioSource.PlayOneShot(audioclip);
    }
}
