using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

/// <summary>BGMを管理するクラス</summary>
public class BGMManager : Singleton<BGMManager>
{
    /// <summary>BGM</summary>
    [SerializeField]
    List<AudioClip> m_BGM;

    /// <summary>オーディオソース</summary>
    AudioSource m_AudioSource;

    private int m_PlayBGMid = 0;

    //ボリュームの最大値を取得
    private float Maxvolume;

    public enum SE
    {
        Jump,
        Landing,
        CheckPoint,
        Door
    }

    void Start()
    {
        //BGMを流す
        m_AudioSource = GetComponent<AudioSource>();
        PlayBGM(m_PlayBGMid);
        
        Maxvolume = m_AudioSource.volume;
    }

    /// <summary>BGMの再生</summary>
    /// <param name="playBgmId">再生するBGMのID</param>
    public void PlayBGM(int playBgmId)
    {
        m_AudioSource.clip = m_BGM[playBgmId];
        m_AudioSource.Play();
        Debug.Log(m_AudioSource.volume);
    }

    //ピッチは音の高さ-3~3
    public void PlaySE(SE SEName, GameObject User, float Volume = 0.6f, float pitch = 1)
    {
        //ASにオーディオソースをつける
        if (User.GetComponent<AudioSource>() == null)
        {
            User.AddComponent<AudioSource>();
        }

        AudioSource audioSource;
        AudioClip Clip;
        string ResName;

        audioSource = User.GetComponent<AudioSource>();
        audioSource.volume = Volume;
        audioSource.pitch = pitch;

        ResName = "SE/" + SEName;
        Clip = Resources.Load(ResName) as AudioClip;

        audioSource.PlayOneShot(Clip);
    }

    public void VolumeFadeOut()
    {
        StartCoroutine("VolumeDown");
    }

    IEnumerator VolumeDown()
    {
        while (m_AudioSource.volume > 0)
        {
            Debug.Log(m_AudioSource.volume);
            m_AudioSource.volume -= Maxvolume / 20f;
            yield return new WaitForSeconds(0.1f);
        }
    }



}
