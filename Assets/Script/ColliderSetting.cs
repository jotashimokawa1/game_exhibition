using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderSetting : MonoBehaviour
{
    [SerializeField] private Collider _colA;
    private MeshCollider _colB;
    

    public MeshCollider colB
    {
        set
        {
            _colB = value;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_colB != null)
        {
            Physics.IgnoreCollision(_colA, _colB, true);
        }

    }
}
