using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class naviText : Singleton<naviText>
{

    Text text;
    
    // Start is called before the first frame update
    void Start()
    {
        text = this.GetComponent<Text>();
    }

    public void SetNaviText(string message)
    {
        text.text = message;
    }
}
