using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OuterFrameRenderer:MonoBehaviour
{
    private Vector2 TopLeft { get; set; }
    private Vector2 TopRight { get; set; }
    private Vector2 BottomLeft { get; set; }
    private Vector2 BottomRight { get; set; }
    private LineRenderer Renderer { get; set; }

    private Vector3[] Vertices { get; set; }

    void Start()
    {
        SetScreenVertices();
        SettingLineRenderer();

        Renderer.SetPositions(Vertices);
    }

    //画面四隅の座標を設定
    private void SetScreenVertices()
    {
        TopLeft = Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height));
        TopRight = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        BottomLeft = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));
        BottomRight = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0));
    }

    //LineRendererの設定
    private void SettingLineRenderer()
    {
        Renderer = GetComponent<LineRenderer>();

        Renderer.startWidth = 0.1f;
        Renderer.endWidth = 0.1f;

        //色の設定
        var gradient = new Gradient();
        var colorKeys = new GradientColorKey[]
        {
           new GradientColorKey(Color.black,0),
           new GradientColorKey(Color.black,1),
        };

        var alphaKeys = new GradientAlphaKey[]
        {
            new GradientAlphaKey(1,0),
            new GradientAlphaKey(1,0),
        };

        gradient.SetKeys(colorKeys, alphaKeys);
        Renderer.colorGradient = gradient;

        //頂点を設定
        Vertices = new Vector3[]
        {
            TopLeft,
            TopRight,
            BottomRight,
            BottomLeft,
            TopLeft,
        };

        Renderer.positionCount = Vertices.Length;
    }
}