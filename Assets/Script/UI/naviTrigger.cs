using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class naviTrigger : MonoBehaviour
{
    [SerializeField] string message;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            naviText.Instance.SetNaviText(message);
        }
    }
}
