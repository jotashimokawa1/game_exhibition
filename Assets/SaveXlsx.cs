using System.IO;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using UnityEditor;
using UnityEngine;

public class SaveXlsx : MonoBehaviour
{
    static readonly string headerPath = "Assets/MapTableHeader.xlsx";
    static readonly string excelPath  = "Assets/OutputTable.xlsx";

    float time = 1f, timer = 0f;
    private void FixedUpdate()
    {
        timer += Time.deltaTime;
        if(timer >= time)
        {
            
            IWorkbook book;

            //using (FileStream stream = new FileStream(headerPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (FileStream stream = new FileStream(excelPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                book = new XSSFWorkbook(stream);
                stream.Close();
            }

            var data = MapTable.Rows;

            var sheet = book.GetSheet("MapTable");

            int lastrow = sheet.LastRowNum + 1;
            int rowIndex = lastrow;
            Debug.Log("excelOpen"+lastrow);

            for (int i = 0; i < data.Count; i++)
            {
                var erow = new ExcelRow(sheet, rowIndex);
                var dataRow = data[i];

                // ��A �� ID
                //erow.CellValue(0, dataRow.ID);
                // ��B �� Type
                //erow.CellValue(1, dataRow.Type.ToString());
                // ��C �� StageNo
                //erow.CellValue(2, dataRow.StageNo);
                //�@��DEF��pos
                erow.CellValue(3, dataRow.pos1);
                // ��GHI��rot
                erow.CellValue(6, dataRow.rot1.eulerAngles);
                //�@��DEF��pos
                erow.CellValue(9, dataRow.pos2);
                // ��GHI��rot
                erow.CellValue(12, dataRow.rot2.eulerAngles);
                //�@��DEF��pos
                erow.CellValue(15, dataRow.pos3);
                // ��GHI��rot
                erow.CellValue(18, dataRow.rot3.eulerAngles);
                //�@��DEF��pos
                erow.CellValue(21, dataRow.pos4);
                // ��GHI��rot
                erow.CellValue(24, dataRow.rot4.eulerAngles);
                rowIndex++;
            }

            File.Delete(excelPath);

            using (FileStream stream = new FileStream(excelPath, FileMode.OpenOrCreate, FileAccess.Write))
            {
                book.Write(stream);
                stream.Close();
            }
            AssetDatabase.ImportAsset(excelPath);

            //timer��0�ɂ���
            timer = 0f;
        }

       

        //EditorUtility.DisplayDialog($"SAVE DATA", $"path: {excelPath}", "ok");
    }
    /*
    [MenuItem ("Tools/SaveXLS")]
    public static void Exec()
    {
        
        IWorkbook book;

        //using (FileStream stream = new FileStream(headerPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        using (FileStream stream = new FileStream(excelPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
            book = new XSSFWorkbook(stream);
            stream.Close();
        }

        var data = MapTable.Rows;

        var sheet = book.GetSheet("MapTable");

        int lastrow  = sheet.LastRowNum+1;
        int rowIndex = lastrow;

        for (int i = 0; i < data.Count; i++)
        {
            var erow    = new ExcelRow(sheet, rowIndex);
            var dataRow = data[i];
            
            // ��A �� ID
            erow.CellValue(0, dataRow.ID);
            // ��B �� Type
            erow.CellValue(1, dataRow.Type.ToString());
            // ��C �� StageNo
            erow.CellValue(2, dataRow.StageNo);
            //�@��DEF��pos
            erow.CellValue(3, dataRow.pos);
            // ��GHI��rot
            erow.CellValue(6, dataRow.rot.eulerAngles);
            rowIndex++;
        }

        File.Delete(excelPath);

        using (FileStream stream = new FileStream(excelPath, FileMode.OpenOrCreate, FileAccess.Write))
        {
            book.Write(stream);
            stream.Close();
        }
        AssetDatabase.ImportAsset(excelPath);

        EditorUtility.DisplayDialog($"SAVE DATA", $"path: {excelPath}", "ok");
    }*/
}

public class ExcelRow
{
    IRow row;

    public ExcelRow(ISheet sheet, int rowIndex)
    {
        row = sheet.GetRow(rowIndex) ?? sheet.CreateRow(rowIndex);
    }

    public void CellValue(int columnIndex, bool value)
    {
        getCell(columnIndex).SetCellValue(value);
    }

    public void CellValue(int columnIndex, System.DateTime value)
    {
        getCell(columnIndex).SetCellValue(value);
    }

    public void CellValue(int columnIndex, int value)
    {
        getCell(columnIndex).SetCellValue(value);
    }

    public void CellValue(int columnIndex, float value)
    {
        getCell(columnIndex).SetCellValue(value);
    }

    public void CellValue(int columnIndex, string value)
    {
        getCell(columnIndex).SetCellValue(value);
    }
    public void CellValue(int columnIndex, Vector3 value)
    {
        getCell(columnIndex).SetCellValue(value.x);
        getCell(columnIndex+1).SetCellValue(value.y);
        getCell(columnIndex+2).SetCellValue(value.z);
    }

    ICell getCell(int columnIndex)
    {
        return row.GetCell(columnIndex) ?? row.CreateCell(columnIndex);
    }
}
